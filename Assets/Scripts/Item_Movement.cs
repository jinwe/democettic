﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Item_Movement : MonoBehaviour {
	public GameManager GM;
	void Start(){
		GM = GameObject.FindGameObjectWithTag ("GM").GetComponent<GameManager> ();
	}
	void Update () {
		if (GM.GameState != 1)
			return;
		
		transform.Translate (Vector3.back * Time.deltaTime * GM.speedPJ * 10);
		if (this.gameObject.transform.position.z <= -10)
			Destroy (this.gameObject);
	}
}
