﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour {

	//COMM-Area - Comunicación entre scripts y servicios.
	public DB_LoginScreen DBLS;
	public DB_getGameSettings DBGS;
	public DB_setSettings DBSS;
	public DB_insertScore DBIS;

	//Vars.
	public int Score;
	public int GameState;

	//GameSettings:
	public int speedPJ;
	public float timeGM,timer;
	public int scorePickup;
	public GameObject Player;

	//UI - Canvas
	public Text TXT_score,TXT_Login;
	public GameObject titulo;
	public Canvas CanvasPersistent,CanvasSettings;

	//Login
	public InputField LOGIN_name,LOGIN_pass;
	public Button LOGIN_button;
	public bool logged = false;
	public string nameRecieved;
	public Text LOG_info;

	//Settings
	public Button SETTINGS_button;
	public Button SAVE_button;
	public Button RESTORE_button;
	public InputField Set_speed, Set_time, Set_score;
	public bool settin = false,readySet = false;
	public int LastState;

	//Exit
	public Button EXIT_button;

	//Start game.
	public Button START_button;
	public bool Ready = false, insScore = false;
	public bool Restart = false;
	GameObject[] clean; // Limpia los clones al reintentar.

	public string l_name, l_pass;

	void Start () {
		CanvasSettings.gameObject.SetActive (false);
		//DEBUG!!!!
		GameState = 0;
		StartCoroutine(DBGS.GetSettings());
	}

	void Update () {
		// Botones GUI
		EXIT_button.onClick.AddListener(CallExit);
		START_button.onClick.AddListener(StartGame);
		SETTINGS_button.onClick.AddListener(SettingsScreen);
		SAVE_button.onClick.AddListener(SaveSettings);
		RESTORE_button.onClick.AddListener(RestoreSettings);

		// STATE = 0: Preparando juego/Pantalla LOGIN.
		if (GameState == 0) {
			if(logged == false)
				LOGIN_button.onClick.AddListener(CallLogin);
		}

		// STATE = 1: El juego corre, se debe inicializar todo lo que ha de correr.
		if (GameState == 1) {
			insScore = false;
			PreparaPantalla ();
			RestaSegundo ();
			// Recupera gameSettings de la BD.
			TXT_score.text = "Time Left: " + (int)timeGM + "\nScore: " + Score;
		} else {
			TXT_score.text = "";
		}

		// STATE = 2: Pantalla de GAME OVER.
		if (GameState == 2) {
			if (insScore == false) {
				StartCoroutine (DBIS.InsertScore ());
				insScore = true;
			}
			//Debug.Log ("Nice try " + nameRecieved + " your score was: " + Score);
			LOG_info.gameObject.SetActive(true);
			LOG_info.fontSize = 30;
			LOG_info.text = ("Nice try " + nameRecieved + ". You scored: " + Score);
			Ready = true;
			START_button.gameObject.SetActive(true);
			SETTINGS_button.gameObject.SetActive(true);
			EXIT_button.gameObject.SetActive(true);
		}

		// STATE = 3: Pantalla de settings.
		if (GameState == 3) {
			CanvasSettings.gameObject.SetActive (true);
			CanvasPersistent.gameObject.SetActive (false);
		}
	}

	// Countdown para terminar el juego.
	void RestaSegundo(){
		timer += Time.deltaTime;
		if ((int)timer == 1) {
			timeGM--;
			if (timeGM <= 0)
				GameState = 2;
			timer = 0;
		}
	}

	// Llamado a función login en la BD
	void CallLogin(){
		if (LOGIN_name.text.Length <= 0 || LOGIN_pass.text.Length <= 0)
			return;
		if (logged == false) {
			l_name = LOGIN_name.text;
			l_pass = LOGIN_pass.text;
			StartCoroutine (DBLS.GetUser ());
			logged = true;
		}
	}

	// Exit game...
	void CallExit(){
		Application.Quit ();
	}

	// Inicializa el juego para comenzar una partida (post login, state = 1)
	void StartGame(){
		if (Ready == true) {
			clean = GameObject.FindGameObjectsWithTag ("clone");
			foreach (GameObject clone in clean) {
				Destroy (clone);
			}
			PreparaPantalla ();
			StartCoroutine(DBGS.GetSettings());
			GameState = 1;
			Score = 0;
			Ready = false;
			settin = false;
		}
	}

	// Pantalla de settings...
	void SettingsScreen(){
		if (settin == false) {
			LastState = GameState;
			GameState = 3;
			settin = true;
			readySet = false;
		}
	}

	// La sección de SETTINGS Podría ser más parametrizable. Funcionalidad sobre efectividad actualmente en aras del tiempo.
	void SaveSettings(){
		if (readySet == false) {
			//Defaults - si el usuario no ingresa uno de los valores genera un string erroneo, para evitar eso, poblamos con el valor anterior.
			if (Set_speed.text.Length <= 0) Set_speed.text = speedPJ.ToString();
			if (Set_time.text.Length <= 0) Set_time.text =  timeGM.ToString();
			if (Set_score.text.Length <= 0) Set_score.text = scorePickup.ToString();

			speedPJ = int.Parse (Set_speed.text);
			timeGM = int.Parse (Set_time.text);
			scorePickup = int.Parse (Set_score.text);
			LOG_info.text = "SETTINGS SAVED!";
			StartCoroutine (DBSS.postUser ());
			CanvasPersistent.gameObject.SetActive (true);
			CanvasSettings.gameObject.SetActive (false);
			settin = true;
			GameState = LastState;
			StartCoroutine (DBGS.GetSettings ());
			readySet = true;
		}
	}

	void RestoreSettings(){
		if (readySet == false) {
			speedPJ = 1;
			timeGM = 30;
			scorePickup = 10;
			StartCoroutine (DBSS.postUser());
			CanvasPersistent.gameObject.SetActive (true);
			CanvasSettings.gameObject.SetActive (false);
			settin = true;
			GameState = LastState;
			StartCoroutine (DBGS.GetSettings ());
			readySet = true;
		}
	}
	// FIN SETTINGS...

	// Sistema de LOGIN - Si no existe lo crea, si la password está mal avisa y si las credenciales están ok "bienvenido de vuelta".
	public void AssignName(){
		if (nameRecieved == "###") {
			LOG_info.text = (l_name + " has been recruited!");
			StartCoroutine (DBLS.postUser ());
			nameRecieved = l_name;
			Ready = true;
		} else if (nameRecieved == "!!!") {
			LOG_info.text = ("Wrong credentials for " + l_name + ".");
			logged = false;
		} else {
			LOG_info.text = ("Welcome back pilot " + nameRecieved + ".");
			Ready = true;
		}
	}

	// Limpia pantalla - Se está usando al salir del estado 1.
	void PreparaPantalla()
	{
		titulo.SetActive (false);
		LOGIN_name.gameObject.SetActive(false);
		LOGIN_pass.gameObject.SetActive(false);
		LOGIN_button.gameObject.SetActive(false);
		LOG_info.gameObject.SetActive(false);
		TXT_Login.gameObject.SetActive (false);
		SETTINGS_button.gameObject.SetActive(false);
		EXIT_button.gameObject.SetActive(false);
		START_button.gameObject.SetActive(false);
	}
}
