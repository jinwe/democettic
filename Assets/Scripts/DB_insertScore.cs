﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DB_insertScore : MonoBehaviour {
	//TAG## - DB_insertScore :: Graba todos los scores al obtener "game over"
	//COMMArea.
	public string SetURL = "localhost/demoCETTIC/scripts_php/insert_score.php?"; 
	public GameManager GM;

	//Vars.
	private string[] StringName;
		public IEnumerator InsertScore()
	{
		string post_url = SetURL + "arg1=" + GM.l_name + "&arg2=" + GM.Score;
		WWW user_post = new WWW(post_url);
		yield return user_post;
		if (user_post.error != null) {
			yield return("Error");
		}
	}
}