﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class floor_spawner : MonoBehaviour {

	public GameManager GM;
	public int side,instance,instItem,offset,shouldInst;
	float posX;
	bool once = false;
	// Use this for initialization
	void Start () {
	}

	void Update(){
		if (GM.GameState != 1){ 
			once = false;
			instance = 0;
			return;
		}			
		
		if (once == false) {
			while (instance <= 4) {
				PutFloor ();
				instance++;
				if (instance >= 4)
					once = true;
			}	
		}
	}

	void PutFloor(){
		instItem = 0;
		offset = 0;
		
		side = (int)Random.Range (1, 4);
		switch (side) {
		case 1:
			posX = -1.5f;
			break;
		case 2:
			posX = 0;
			break;
		case 3:
			posX = 1.5f;
			break;
		}

		GameObject floor = Instantiate(Resources.Load("floor",typeof(GameObject))) as GameObject;
		floor.transform.position = new Vector3 (posX, transform.position.y, 20 * instance);

		
		if(instance == 0){
			GameObject player = Instantiate(Resources.Load("player",typeof(GameObject))) as GameObject;
			player.transform.position = new Vector3(floor.transform.position.x, floor.transform.position.y + 1f, floor.transform.position.z -5);
		}
	}
}
