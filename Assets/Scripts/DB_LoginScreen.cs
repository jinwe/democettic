﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DB_LoginScreen : MonoBehaviour {
	//TAG## - DB_LoginScreen :: Contiene las funciones relacionadas a lectura/consulta de pilotos en la tabla users.
	//COMMArea.
	public string LoginURL = "localhost/demoCETTIC/scripts_php/select_user.php?"; 
	public string CreateURL = "localhost/demoCETTIC/scripts_php/create_user.php?"; 
	public GameManager GM;

	//Vars.
	private string[] StringName;

	//TAG## - GetUser: realiza la consulta de un piloto.
	public IEnumerator GetUser()
	{
		string get_url = LoginURL + "arg1=" + GM.l_name + "&arg2=" + GM.l_pass;
		WWW user_get = new WWW(get_url);
		yield return user_get;
		GM.nameRecieved = user_get.text;
		GM.AssignName (); // Una vez capturado el nombre, lo devuelve a GM.
	}

	//TAG## - postUser: Si es un piloto nuevo, lo crea.
	public IEnumerator postUser()
	{
		string post_url = CreateURL + "arg1=" + GM.l_name + "&arg2=" + GM.l_pass;
		WWW user_post = new WWW(post_url);
		yield return user_post;
	}
}

