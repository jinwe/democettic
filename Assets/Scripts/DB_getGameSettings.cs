﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DB_getGameSettings : MonoBehaviour {
	//TAG## - DB_getGameSettings :: Contiene la función encargada de recuperar los parámetros de "dificultad".
	//COMMArea.
	public string GameSettingsURL = "localhost/demoCETTIC/scripts_php/get_settings.php";
	public GameManager GM;

	//Vars.
	private string[] StringSettings;

	public IEnumerator GetSettings()
    {
		string get_url = GameSettingsURL;
 

        WWW gameSettings_get = new WWW(get_url);
		yield return gameSettings_get; 
		if (gameSettings_get.error != null) {
			yield return("1;30;10"); //Speed, time, score. - En caso de error devuelve valores default.
		} else {
			// Recuperó data de la BD sin errores. Puebla los campos. - Se podría JSONificar, pero queda como TODO.
			StringSettings = gameSettings_get.text.Split(';');
			GM.speedPJ     = int.Parse(StringSettings[0]);
			GM.timeGM      = int.Parse(StringSettings[1]);
			GM.scorePickup = int.Parse(StringSettings[2]);
		}
    }
}

