﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour {

	public GameManager GM;
	public GameObject pos;
	public CamShake CS;
	public bool restart = false;
	Rigidbody RB; 
	int fuelJump;

	public AudioClip thum;
	public AudioSource ASrcPlayer;

	void Start () {
		GM = GameObject.FindGameObjectWithTag ("GM").GetComponent<GameManager>();
		RB = GetComponent<Rigidbody> ();
		CS = GameObject.FindGameObjectWithTag ("MainCamera").GetComponent<CamShake> ();
		ASrcPlayer = GetComponent<AudioSource> ();
	}
	
	// Update is called once per frame
	void Update () {
		if (transform.position.z != -5)
			transform.position = new Vector3 (transform.position.x, transform.position.y, -5);
		
		if (transform.position.y <= -3) {
			GM.GameState = 2;
		}


		// Controles.
		if (Input.GetKey ("space")) {
			if (fuelJump > 0) {
				RB.AddForce (Vector3.up * 10);
				fuelJump--;
			}
		}

		if (Input.GetKey ("left")) {
			transform.Translate (Vector3.left * Time.deltaTime * 1);
		}

		if (Input.GetKey ("right")) {
			transform.Translate (Vector3.right * Time.deltaTime * 1);
		}
	}

	void OnCollisionEnter(Collision col)
	{
		if (col.gameObject.name == "floor(Clone)"){
			fuelJump = 100;
			CS.shakeDuration = 0.3f;
		}
		if (col.gameObject.name == "Item(Clone)") { // Es arcaico, podría ser un tag en el prefab, pero buscar tags aparentemente es más caro que buscar nombres.
			ASrcPlayer.clip = thum;
			ASrcPlayer.PlayOneShot (thum, 0.05f);
			Destroy (col.gameObject);
			GameObject Xplode = Instantiate(Resources.Load("explo",typeof(GameObject))) as GameObject;
			Xplode.transform.position = this.transform.position;
			GM.Score += GM.scorePickup;
			GM.timeGM++; // Le suma 1 al tiempo de vida, si no es muy fome el juego.
		}
	}


}
