﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DB_setSettings : MonoBehaviour {
	//TAG## - DB_setSettings :: Escribe los settings del juego en la BD.
	//COMMArea.
	public string SetURL = "localhost/demoCETTIC/scripts_php/set_settings.php?"; 
	public GameManager GM;

	//Vars.
	private string[] StringName;

	public IEnumerator postUser()
	{
		string post_url = SetURL + "arg1=" + GM.speedPJ + "&arg2=" + GM.timeGM + "&arg3=" + GM.scorePickup;
		WWW user_post = new WWW(post_url);
		yield return user_post;
		if (user_post.error != null) {
			yield return("Error"); //Speed, time, score. - En caso de error devuelve valores default.
		} else {
			GM.settin = true;
		}
	}
}

