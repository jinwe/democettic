﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class floor_movement : MonoBehaviour {

	public GameManager GM;
	public int side; // Random de 3 que determina la posición
	public int instItem,offset,shouldInst;
	public float posX;
	// Use this for initialization
	void Start () {
		offset = 2;
		GM = GameObject.FindGameObjectWithTag ("GM").GetComponent<GameManager>();
	}
	
	// Update is called once per frame
	void Update () {
		if (GM.GameState != 1)
			return;
		
			transform.Translate (Vector3.back * Time.deltaTime * GM.speedPJ * 10);
			if (transform.position.z <= -20) {
				side = (int)Random.Range (1, 4);
				switch (side) {
				case 1:
					posX = -1.5f;
					break;
				case 2:
					posX = 0;
					break;
				case 3:
					posX = 1.5f;
					break;
				}

				if (transform.position.z <= 30) {
					transform.position = new Vector3 (posX, transform.position.y, 60);
					shouldInst = (int)Random.Range (1, 4);
					if (shouldInst == 2) {
						while (instItem <= 5) {
							GameObject Item = Instantiate (Resources.Load ("Item", typeof(GameObject))) as GameObject;
							Item.transform.position = new Vector3 (this.transform.position.x, this.transform.position.y + 1, this.transform.position.z + offset);
							instItem++;
							offset += 2;
						}
					}

					if (instItem >= 5)
						instItem = 0;
				}
			}
	}
}
