﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CamMusic : MonoBehaviour {

	public GameManager GM;
	public AudioClip musica;
	AudioSource Aud;
	// Use this for initialization
	void Start () {
		Aud = GetComponent<AudioSource> ();
	}
	
	// Update is called once per frame
	void Update () {
		if (GM.GameState == 0 || GM.GameState == 1)
			Aud.volume = 0.15f;
		else
			Aud.volume = 0;
	}
}
